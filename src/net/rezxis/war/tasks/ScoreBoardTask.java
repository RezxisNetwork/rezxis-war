package net.rezxis.war.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import lombok.Getter;
import net.rezxis.war.Config;
import net.rezxis.war.enums.GameStatus;
import net.rezxis.war.manager.GameManager;
import net.rezxis.war.manager.TeamManager;

public class ScoreBoardTask extends BukkitRunnable {

	@Getter
	private static ScoreBoardTask instance;
	
	@Getter
	private Scoreboard board;
	
	private Objective obj;
	
	public ScoreBoardTask() {
		instance = this;
	}
	
	@Override
	public void run() {
		if (board == null) {
			board = Bukkit.getScoreboardManager().getNewScoreboard();
		}
		if (obj == null) {
			obj = board.registerNewObjective("info", "dummy");
		} else {
			obj.unregister();
			obj = board.registerNewObjective("info", "dummy");
		}
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.setDisplayName(ChatColor.GREEN+"["+ChatColor.RED+ChatColor.BOLD+"攻城戦"+ChatColor.RESET+ChatColor.GREEN+"]");
		obj.getScore(ChatColor.GREEN+"進行状況 : "+GameManager.getInstance().getStatus().getDesc()).setScore(9);
		obj.getScore(" ").setScore(8);
		if (GameManager.getInstance().getStatus() == GameStatus.WAITING) {
			if (TeamManager.getInstance().getJoinnedPlayers() >= Config.getInstance().getStartPlayer()) {
				obj.getScore(ChatColor.GOLD+"スタートまで : "+WaitingTask.getInstance().getTime()+"秒").setScore(7);
			} else {
				obj.getScore(ChatColor.GOLD+"必要人数 : "+TeamManager.getInstance().getJoinnedPlayers()+"/"+Config.getInstance().getStartPlayer()).setScore(7);
			}
		} else if (GameManager.getInstance().getStatus() == GameStatus.BUILD) {
			obj.getScore(ChatColor.GOLD+"建築残り時間 : "+(BuildRepeatingTask.getInstance().getTime()-BuildRepeatingTask.getInstance().getTime()%60)/60+"分"+BuildRepeatingTask.getInstance().getTime()%60+"秒").setScore(7);
		} else if (GameManager.getInstance().getStatus() == GameStatus.PVP) {
			obj.getScore(ChatColor.GOLD+"経過時間 : "+(PVPRepeatingTask.getInstance().getTime()-PVPRepeatingTask.getInstance().getTime()%60)/60+"分"+PVPRepeatingTask.getInstance().getTime()%60+"秒").setScore(7);;
		}
	}
}
