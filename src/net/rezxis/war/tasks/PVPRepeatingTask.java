package net.rezxis.war.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import lombok.Getter;
import lombok.Setter;

public class PVPRepeatingTask extends BukkitRunnable {

	@Getter
	private static PVPRepeatingTask instance;
	@Getter
	private int time = 0;
	@Setter
	private boolean kill;
	
	public PVPRepeatingTask() {
		instance = this;
	}
	
	public void reset() {
		this.kill = false;
		this.time = 0;
	}
	
	@Override
	public void run() {
		if (kill)
			return;
		time+=1;
	}
}
