package net.rezxis.war.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.rezxis.war.Config;
import net.rezxis.war.manager.GameManager;
import net.rezxis.war.manager.TeamManager;
import net.rezxis.war.utils.ChatUtil;

public class WaitingTask extends BukkitRunnable {

	@Getter
	private static WaitingTask instance;
	private boolean countdown = false;
	@Getter
	private int time;
	private boolean kill = false;
	
	public WaitingTask() {
		instance = this;
		time = Config.getInstance().getCountToStart();
	}
	
	public void reset() {
		this.time = Config.getInstance().getCountToStart();
		this.countdown = false;
		kill = false;
	}
	
	@Override
	public void run() {
		if (kill)
			return;
		if (TeamManager.getInstance().getJoinnedPlayers() >= Config.getInstance().getStartPlayer()) {
			if (!countdown) {
				countdown = true;
				ChatUtil.alertPrefix(ChatColor.GREEN+"カウントダウンが始まりました。開始まで、後"+time+"秒！");
				return;
			}
			if (time == 0) {
				GameManager.getInstance().startGame();
				kill = true;
				return;
			}
			if (time <= 10) {
				count();
			} else {
				if (time % 10 == 0) {
					count();
				}
			}
			time -= 1;
		} else {
			if (countdown) {
				countdown = false;
				time = Config.getInstance().getCountToStart();
				ChatUtil.alertPrefix(ChatColor.RED+"最低開始人数"+Config.getInstance().getStartPlayer()+"以下なので、開始はキャンセルされました。");
			}
		}
	}
	
	private void count() {
		ChatUtil.alertPrefix(ChatColor.GREEN+"開始まで、後"+time+"秒");
	}
}
