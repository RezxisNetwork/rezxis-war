package net.rezxis.war.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import lombok.Getter;
import net.rezxis.war.Config;
import net.rezxis.war.manager.GameManager;

public class BuildRepeatingTask extends BukkitRunnable {
	
	@Getter
	private static BuildRepeatingTask instance;
	@Getter
	private int time;
	private boolean kill = false;
	
	
	public BuildRepeatingTask() {
		instance = this;
		time = Config.getInstance().getBuildTime();
	}
	
	public void reset() {
		time = Config.getInstance().getBuildTime();
		kill = false;
	}
	
	@Override
	public void run() {
		if (kill)
			return;
		if (time == 0) {
			GameManager.getInstance().chanceToPVPTime();
			kill = true;
			return;
		}
		time -=1;
	}
}
