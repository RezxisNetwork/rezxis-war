package net.rezxis.war;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import net.rezxis.war.command.CommandHandler;
import net.rezxis.war.manager.GameManager;
import net.rezxis.war.manager.KitManager;
import net.rezxis.war.manager.PlayerManager;
import net.rezxis.war.manager.TeamManager;
import net.rezxis.war.object.PlayerListener;
import net.rezxis.war.tasks.ScoreBoardTask;
import net.rezxis.war.tasks.WaitingTask;
import net.rezxis.war.utils.MapUtil;
import net.rezxis.war.utils.TitleUtil;

@Getter
public class TheWarPlugin extends JavaPlugin {

	@Getter
	private static TheWarPlugin instance;
	private PlayerManager playerManager;
	private TeamManager teamManager;
	private GameManager gameManager;
	private KitManager kitManager;
	private Config gConfig;
	private MapUtil mapUtil;
	private TitleUtil titleUtil;
	
	public void onEnable() {
		instance = this;
		initUtils();
		initManagers();
		registerTasks();
		Bukkit.getWorld(gConfig.getWorld()).setGameRuleValue("doMobSpawning", "false");
		Bukkit.getWorld(gConfig.getWorld()).setSpawnLocation(gConfig.getLobby());
		this.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
	}
	
	private void initUtils() {
		this.gConfig = new Config();
		this.mapUtil = new MapUtil(this);
		this.titleUtil = new TitleUtil();
	}
	
	private void initManagers() {
		this.playerManager = new PlayerManager();
		this.teamManager = new TeamManager();
		this.gameManager = new GameManager();
		this.kitManager = new KitManager();
	}
	
	@SuppressWarnings("deprecation")
	private void registerTasks() {
		this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new WaitingTask(), 0, 20L);
		this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new ScoreBoardTask(), 0, 20L);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		return CommandHandler.onCommand(sender, cmd, commandLabel, args);
	}
}
