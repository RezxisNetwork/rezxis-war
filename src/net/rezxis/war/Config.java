package net.rezxis.war;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import lombok.Getter;

public class Config {

	@Getter
	private static Config instance;
	
	@Getter
	private int startPlayer = 4;
	@Getter
	private int countToStart = 60;
	@Getter
	private String world = "world";
	@Getter
	private Location mapPos = new Location(Bukkit.getWorld(world), 0, 20, 5);
	@Getter
	private Location lobby = new Location(Bukkit.getWorld(world), 0, 6, 0);
	@Getter
	private int side = 30;
	@Getter
	private int vertical = 30;
	@Getter
	private int height = 45;
	@Getter
	private int buildTime = 600;
	
	public Config() {
		instance = this;
	}
}
