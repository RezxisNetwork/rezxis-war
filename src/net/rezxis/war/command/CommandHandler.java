package net.rezxis.war.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rezxis.war.enums.Team;
import net.rezxis.war.manager.GameManager;
import net.rezxis.war.manager.PlayerManager;
import net.rezxis.war.manager.TeamManager;
import net.rezxis.war.utils.ChatUtil;

public class CommandHandler {

	public static boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("game")) {
			if (args.length == 0) {
				help((Player)sender);
			} else {
				if (args[0].equalsIgnoreCase("start")) {
					GameManager.getInstance().startGame();
				} else if (args[0].equalsIgnoreCase("join")) {
					Player target = (Player)sender;
					if (sender.isOp() && args.length == 2) {
						target = Bukkit.getPlayer(args[1]);
					}
					if (TeamManager.getInstance().getRedPlayers().size() == TeamManager.getInstance().getBluePlayers().size()) {
						if (TeamManager.getInstance().join(PlayerManager.getInstance().getPlayer(target), Team.RED)) {
							target.sendMessage(ChatColor.GREEN+"あなたは"+ChatColor.RED+"REDに参加しました。");
							ChatUtil.alertPrefix(ChatColor.RED+target.getName()+"がREDに参加しました。");
						} else {
							target.sendMessage(ChatColor.RED+"あなたはすでに参加しています。");
						}
					} else {
						if (TeamManager.getInstance().join(PlayerManager.getInstance().getPlayer(target), Team.BLUE)) {
							target.sendMessage(ChatColor.GREEN+"あなたは"+ChatColor.BLUE+"BLUEに参加しました。");
							ChatUtil.alertPrefix(ChatColor.BLUE+target.getName()+"がBLUEに参加しました。");
						} else {
							target.sendMessage(ChatColor.RED+"あなたはすでに参加しています。");
						}
					}
				} else if (args[0].equalsIgnoreCase("kill")) {
					((Player)sender).damage(1000);
				} else {
					help((Player)sender);
				}
			}
		}
		return true;
	}
	
	public static void help(Player player) {
		player.sendMessage(ChatColor.GRAY+"--------============"+ChatColor.RED+"攻城戦"+ChatColor.GRAY+"============--------");
		player.sendMessage(ChatColor.AQUA+"/game start : ゲームを開始します。");
		player.sendMessage(ChatColor.AQUA+"/game kill : 自殺します。");
	}
}
