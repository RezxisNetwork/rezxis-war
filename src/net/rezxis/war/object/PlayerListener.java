package net.rezxis.war.object;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import lombok.Getter;
import net.rezxis.war.Config;
import net.rezxis.war.enums.GameStatus;
import net.rezxis.war.manager.GameManager;
import net.rezxis.war.manager.PlayerManager;
import net.rezxis.war.tasks.ScoreBoardTask;

public class PlayerListener implements Listener {

	@Getter
	private static PlayerListener instance;
	
	public PlayerListener() {
		instance = this;
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		event.getPlayer().setScoreboard(ScoreBoardTask.getInstance().getBoard());
		event.getPlayer().getInventory().clear();
		event.getPlayer().updateInventory();
		event.getPlayer().setGameMode(org.bukkit.GameMode.SURVIVAL);
		event.getPlayer().teleport(Config.getInstance().getLobby());
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event)  {
		
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		
	}
	
	@EventHandler
	public void onAttacked(EntityDamageByEntityEvent event) {
		if (GameManager.getInstance().getStatus() == GameStatus.PVP) {
			if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
				WarPlayer damager = PlayerManager.getInstance().getPlayer((Player)event.getDamager());
				WarPlayer target = PlayerManager.getInstance().getPlayer((Player)event.getEntity());
				if (target.getTeam() == damager.getTeam()) {
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if (GameManager.getInstance().getStatus() == GameStatus.BUILD) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		if (GameManager.getInstance().getStatus() == GameStatus.PVP) {
			GameManager.getInstance().getGame().onRespawn(event);
		} else {
			event.setRespawnLocation(Config.getInstance().getLobby());
		}
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		event.setKeepInventory(true);
		GameManager.getInstance().getGame().onDeath(event);
	}
}