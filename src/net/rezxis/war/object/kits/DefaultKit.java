package net.rezxis.war.object.kits;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import net.rezxis.war.object.Kit;

public class DefaultKit extends Kit {

	public DefaultKit() {
		super("Default",ChatColor.GREEN+"デフォルト");
	}

	@Override
	public HashMap<Integer, ItemStack> getBuildItems() {
		HashMap<Integer, ItemStack> map = new HashMap<Integer, ItemStack>();
		map.put(0,new ItemStack(Material.DIAMOND_PICKAXE));
		map.put(1,new ItemStack(Material.DIAMOND_AXE));
		map.put(2,new ItemStack(Material.DIAMOND_SPADE));
		map.put(3,new ItemStack(Material.DIAMOND_HOE));
		ItemStack wood = new ItemStack(Material.WOOD);
		wood.setAmount(64);
		map.put(4, wood);
		map.put(5, wood);
		ItemStack cobble = new ItemStack(Material.COBBLESTONE);
		cobble.setAmount(64);
		map.put(6, cobble);
		ItemStack steak = new ItemStack(Material.COOKED_BEEF);
		steak.setAmount(64);
		map.put(8, steak);
		return map;
	}

	@Override
	public HashMap<Integer, ItemStack> getPvpItems() {
		HashMap<Integer, ItemStack> map = new HashMap<Integer, ItemStack>();
		map.put(0, new ItemStack(Material.WOOD_SWORD));
		map.put(1, new ItemStack(Material.BOW));
		ItemStack arrow = new ItemStack(Material.ARROW);
		arrow.setAmount(64);
		map.put(2, arrow);
		ItemStack steak = new ItemStack(Material.COOKED_BEEF);
		steak.setAmount(64);
		map.put(8, steak);
		return map;
	}
}
