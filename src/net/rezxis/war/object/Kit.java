package net.rezxis.war.object;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;

public abstract class Kit {

	@Getter
	private String name;
	@Getter
	private String display;
	
	public Kit(String name, String display) {
		this.name = name;
		this.display = display;
	}
	
	public void giveBuildItems() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.getInventory().clear();
			for (Entry<Integer,ItemStack> e : getBuildItems().entrySet()) {
				player.getInventory().setItem(e.getKey(), e.getValue());
			}
			player.updateInventory();
		}
	}
	public void givePVPItems() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.getInventory().clear();
			for (Entry<Integer,ItemStack> e : getPvpItems().entrySet()) {
				player.getInventory().setItem(e.getKey(), e.getValue());
			}
			player.updateInventory();
		}
	}
	
	public abstract HashMap<Integer,ItemStack> getBuildItems();
	
	public abstract HashMap<Integer,ItemStack> getPvpItems();
}
