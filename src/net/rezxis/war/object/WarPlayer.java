package net.rezxis.war.object;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;
import net.rezxis.war.enums.Team;
import net.rezxis.war.manager.TeamManager;

public class WarPlayer {

	@Getter
	private UUID uuid;
	@Getter@Setter
	private Team team;
	@Getter@Setter
	private boolean living;
	@Getter@Setter
	private boolean king;
	
	public WarPlayer(UUID uuid) {
		this.uuid = uuid;
	}
	
	public void join(Team team) {
		this.team = team;
		TeamManager.getInstance().join(this, team);
	}
}
