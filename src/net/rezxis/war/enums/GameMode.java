package net.rezxis.war.enums;

import lombok.Getter;

public enum GameMode {

	DeathMatch("攻防戦","相手のチームを滅ぼすと勝ちです。"),KING("攻王戦","王が倒されると負けです。");
	
	@Getter
	String name;
	@Getter
	String desc;
	GameMode(String name, String desc) {
		this.name = name;
		this.desc = desc;
	}
}
