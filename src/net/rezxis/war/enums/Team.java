package net.rezxis.war.enums;

import net.md_5.bungee.api.ChatColor;

public enum Team {

	RED(ChatColor.RED,"RED",0),BLUE(ChatColor.BLUE,"BLUE",1);
	
	ChatColor color;
	String name;
	int id;
	
	Team(ChatColor c, String n, int i) {
		this.color = c;
		this.name = n;
		this.id = i;
	}
}
