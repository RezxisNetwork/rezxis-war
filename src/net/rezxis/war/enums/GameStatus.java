package net.rezxis.war.enums;

import lombok.Getter;

public enum GameStatus {
	WAITING("待機中"),STARTING("開始中"),BUILD("建築時間"),PVP("対戦時間"),STOPPING("終了中");
	
	@Getter
	String desc;
	GameStatus(String desc) {
		this.desc = desc;
	}
}
