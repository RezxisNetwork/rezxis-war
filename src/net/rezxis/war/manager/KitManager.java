package net.rezxis.war.manager;

import java.util.HashMap;

import lombok.Getter;
import net.rezxis.war.object.Kit;
import net.rezxis.war.object.kits.DefaultKit;

public class KitManager {

	@Getter
	private static KitManager instance;
	
	private HashMap<String,Kit> kits;
	
	public KitManager() {
		instance = this;
		register();
	}
	
	public void add(Kit kit) {
		kits.put(kit.getName(), kit);
		System.out.println("registed : "+kit.getName());
	}
	
	public int getKitSize() {
		return kits.size();
	}
	
	public Kit getKit(int i) {
		return (Kit) kits.values().toArray()[i];
	}
	
	public Kit getKit(String kit) {
		return kits.get(kit);
	}
	
	public void register() {
		if (kits != null) {
			kits.clear();
		} else {
			kits = new HashMap<>();
		}
		add(new DefaultKit());
	}
}
