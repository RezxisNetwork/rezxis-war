package net.rezxis.war.manager;

import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.rezxis.war.Config;
import net.rezxis.war.TheWarPlugin;
import net.rezxis.war.enums.GameMode;
import net.rezxis.war.enums.GameStatus;
import net.rezxis.war.enums.Team;
import net.rezxis.war.manager.game.IGame;
import net.rezxis.war.manager.game.games.DeathMatch;
import net.rezxis.war.manager.game.games.KingGame;
import net.rezxis.war.object.Kit;
import net.rezxis.war.object.WarPlayer;
import net.rezxis.war.tasks.BuildRepeatingTask;
import net.rezxis.war.tasks.PVPRepeatingTask;
import net.rezxis.war.tasks.WaitingTask;
import net.rezxis.war.utils.ChatUtil;
import net.rezxis.war.utils.MapUtil;

public class GameManager {

	@Getter
	private static GameManager instance;
	@Getter@Setter
	private GameStatus status;
	@Getter
	private GameMode mode;
	@Getter
	private Location red;
	@Getter
	private Location blue;
	@Getter
	private Kit kit;
	@Getter
	private IGame game;
	
	public GameManager() {
		instance = this;
		status = GameStatus.WAITING;
	}
	
	@SuppressWarnings("deprecation")
	public void startGame() {
		if (status != GameStatus.WAITING)
			return;
		ChatUtil.alertPrefix(ChatColor.GREEN+"ゲームを開始します！");
		status = GameStatus.STARTING;
		clearMap();
		initMap();
		updateGround();
		placeSplit();
		placeRoof();
		red = Config.getInstance().getMapPos().clone().add(1,5,1+Config.getInstance().getSide()/2);
		blue = red.clone().add(1+Config.getInstance().getVertical() *2 - 10, 0, 0);
		mode = GameMode.values()[new Random().nextInt(GameMode.values().length-1)];
		ChatUtil.alertPrefix(ChatColor.GREEN+"今回のゲームモードは、"+ChatColor.RED+mode.getName()+ChatColor.GREEN+"です！");
		ChatUtil.alert(ChatColor.GOLD+mode.getDesc());
		if (KitManager.getInstance().getKitSize() == 1) {
			kit = KitManager.getInstance().getKit(0);
		} else {
			kit = KitManager.getInstance().getKit(new Random().nextInt(KitManager.getInstance().getKitSize()-1));
		}
		if (mode == GameMode.DeathMatch) {
			game = new DeathMatch();
		} else if (mode == GameMode.KING) {
			game = new KingGame();
		}
		ChatUtil.alertPrefix(ChatColor.GREEN+"今回のKitは、"+kit.getDisplay()+ChatColor.GREEN+"です！");
		game.start();
		status = GameStatus.BUILD;
		
		for (WarPlayer wp : TeamManager.getInstance().getAllPlayers()) {
			wp.setLiving(true);
			Player player = Bukkit.getPlayer(wp.getUuid());
			if (wp.getTeam() == Team.RED) {
				player.teleport(red);
			} else {
				player.teleport(blue);
			}
			player.setGameMode(org.bukkit.GameMode.SURVIVAL);
		}
		kit.giveBuildItems();
		if (BuildRepeatingTask.getInstance() == null) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(TheWarPlugin.getInstance(), new BuildRepeatingTask(), 0L, 20L);
		} else {
			BuildRepeatingTask.getInstance().reset();
		}
	}
	
	public void finishGame() {
		this.status = GameStatus.STOPPING;
		game.finish();
		PVPRepeatingTask.getInstance().setKill(true);
		kit = null;
		mode = null;
		red = null;
		blue = null;
		PlayerManager.getInstance().clear();
		TeamManager.getInstance().clear();
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.teleport(Config.getInstance().getLobby());
			player.setGameMode(org.bukkit.GameMode.SURVIVAL);
			player.getInventory().clear();
		}
		this.status = GameStatus.WAITING;
		WaitingTask.getInstance().reset();
	}
	
	@SuppressWarnings("deprecation")
	public void chanceToPVPTime() {
		if (PVPRepeatingTask.getInstance() == null) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(TheWarPlugin.getInstance(), new PVPRepeatingTask(), 0L, 20L);
		} else {
			PVPRepeatingTask.getInstance().reset();
		}
		status = GameStatus.PVP;
		deleteSplit();
		for (WarPlayer wp : TeamManager.getInstance().getAllPlayers()) {
			Player player = Bukkit.getPlayer(wp.getUuid());
			player.setGameMode(org.bukkit.GameMode.ADVENTURE);
			player.getInventory().clear();
			for (Entry<Integer,ItemStack> e : kit.getPvpItems().entrySet()) {
				player.getInventory().setItem(e.getKey(), e.getValue());
			}
			player.updateInventory();
		}
		ChatUtil.alertPrefix(ChatColor.GOLD+"ゲームスタート！");
	}
	
	public void clearMap() {
		Location pos0 = Config.getInstance().getMapPos().clone();
		Location pos1 = Config.getInstance().getMapPos().clone().add(3+Config.getInstance().getVertical() * 2,Config.getInstance().getHeight(),Config.getInstance().getSide()+2);
		MapUtil.getInstance().fillSync(pos0, pos1, Material.AIR);
	}
	
	public void initMap() {
		//外壁を立てる
		Location wall0 = Config.getInstance().getMapPos().clone().add(3, 0, 0).add(Config.getInstance().getVertical() * 2, Config.getInstance().getHeight(), 0);
		MapUtil.getInstance().fillSync(Config.getInstance().getMapPos(), wall0, Material.BEDROCK);
		Location wall1 = wall0.clone().add(0, -Config.getInstance().getHeight(), 2).add(0, 0, Config.getInstance().getSide());
		MapUtil.getInstance().fillSync(wall0, wall1, Material.BEDROCK);
		Location wall2 = wall1.clone().add(-3, Config.getInstance().getHeight(), 0).add(Config.getInstance().getVertical() * -2, 0, 0);
		MapUtil.getInstance().fillSync(wall1, wall2, Material.BEDROCK);
		MapUtil.getInstance().fillSync(Config.getInstance().getMapPos(), wall2, Material.BEDROCK);
		Location botton = Config.getInstance().getMapPos().clone().add(3+Config.getInstance().getVertical() * 2, 0, Config.getInstance().getSide()+2);
		MapUtil.getInstance().fillSync(Config.getInstance().getMapPos(), botton, Material.BEDROCK);
	}
	
	public void updateGround() {
		Location pos0 = Config.getInstance().getMapPos().clone().add(1, 1, 1);
		Location pos1 = Config.getInstance().getMapPos().clone().add(2+Config.getInstance().getVertical() * 2, 3, Config.getInstance().getSide()+1);
		Location pos2 = Config.getInstance().getMapPos().clone().add(1, 4, 1);
		Location pos3 = Config.getInstance().getMapPos().clone().add(2+Config.getInstance().getVertical() * 2, 4, Config.getInstance().getSide()+1);
		MapUtil.getInstance().fillSync(pos0, pos1, Material.DIRT);
		MapUtil.getInstance().fillSync(pos2, pos3, Material.GRASS);
	}
	
	public void deleteSplit() {
		Location pos0 = Config.getInstance().getMapPos().clone().add(2+Config.getInstance().getVertical(),4,1);
		Location pos1 = pos0.clone().add(0, Config.getInstance().getHeight()-1, Config.getInstance().getSide());
		MapUtil.getInstance().fillSync(pos0, pos1, Material.AIR);
	}
	
	public void placeSplit() {
		Location pos0 = Config.getInstance().getMapPos().clone().add(2+Config.getInstance().getVertical(),1,1);
		Location pos1 = pos0.clone().add(0, Config.getInstance().getHeight()-1, Config.getInstance().getSide());
		MapUtil.getInstance().fillSync(pos0, pos1, Material.BEDROCK);
	}
	
	public void placeRoof() {
		Location pos0 = Config.getInstance().getMapPos().clone().add(0, Config.getInstance().getHeight()+1, 0);
		Location pos1 = Config.getInstance().getMapPos().clone().add(3+Config.getInstance().getVertical() * 2, Config.getInstance().getHeight()+1, Config.getInstance().getSide()+2);
		MapUtil.getInstance().fillSync(pos0, pos1, Material.BARRIER);
	}
}