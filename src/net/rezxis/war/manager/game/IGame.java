package net.rezxis.war.manager.game;

import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import lombok.Getter;
import net.rezxis.war.enums.GameMode;

public abstract class IGame {

	@Getter
	private GameMode mode;
	
	public IGame(GameMode mode) {
		this.mode = mode;
	}
	
	public abstract void start();
	
	public abstract void pvp();
	
	public abstract void finish();
	
	public abstract void onRespawn(PlayerRespawnEvent event);
	public abstract void onDeath(PlayerDeathEvent event);
}
