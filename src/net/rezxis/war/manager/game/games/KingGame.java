package net.rezxis.war.manager.game.games;

import java.util.Random;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.rezxis.war.enums.GameMode;
import net.rezxis.war.enums.Team;
import net.rezxis.war.manager.GameManager;
import net.rezxis.war.manager.PlayerManager;
import net.rezxis.war.manager.TeamManager;
import net.rezxis.war.manager.game.IGame;
import net.rezxis.war.object.WarPlayer;
import net.rezxis.war.utils.ChatUtil;
import net.rezxis.war.utils.TitleUtil;

public class KingGame extends IGame {

	@Getter
	private WarPlayer redKing;
	@Getter
	private WarPlayer blueKing;
	
	public KingGame() {
		super(GameMode.KING);
	}

	@Override
	public void start() {
		if (TeamManager.getInstance().getRedPlayers().size() == 1) {
			redKing = TeamManager.getInstance().getRedPlayers().get(0);
		} else {
			redKing = TeamManager.getInstance().getRedPlayers().get(new Random().nextInt(TeamManager.getInstance().getRedPlayers().size()-1));
		}
		if (TeamManager.getInstance().getBluePlayers().size() == 1) {
			redKing = TeamManager.getInstance().getBluePlayers().get(0);
		} else {
			redKing = TeamManager.getInstance().getBluePlayers().get(new Random().nextInt(TeamManager.getInstance().getRedPlayers().size()-1));
		}
		redKing.setKing(true);
		blueKing.setKing(true);
		ChatUtil.alertPrefix(ChatColor.RED+"REDチームの王は"+Bukkit.getPlayer(redKing.getUuid()).getName()+"です。");
		ChatUtil.alertPrefix(ChatColor.BLUE+"BLUEチームの王は"+Bukkit.getPlayer(blueKing.getUuid()).getName()+"です。");
	}

	@Override
	public void pvp() {
		
	}
	
	@Override
	public void finish() {
		
	}

	@Override
	public void onRespawn(PlayerRespawnEvent event) {
		WarPlayer wp = PlayerManager.getInstance().getPlayer(event.getPlayer());
		if (wp.getTeam() == Team.RED) {
			event.setRespawnLocation(GameManager.getInstance().getRed());
		} else {
			event.setRespawnLocation(GameManager.getInstance().getBlue());
		}
		event.getPlayer().getInventory().clear();
		for (Entry<Integer,ItemStack> e : GameManager.getInstance().getKit().getBuildItems().entrySet()) {
			event.getPlayer().getInventory().setItem(e.getKey(), e.getValue());
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		WarPlayer wp = PlayerManager.getInstance().getPlayer(event.getEntity());
		if (wp.isKing()) {
			String title;
			if (wp.getTeam() == Team.RED) {
				title = ChatColor.BLUE+"BLUEチーム"+ChatColor.GOLD+"の勝利です！";
			} else {
				title = ChatColor.RED+"REDチーム"+ChatColor.GOLD+"の勝利です！";
			}
			for (Player p : Bukkit.getOnlinePlayers()) {
				TitleUtil.getInstance().sendTitle(p, title, null, null);
				TitleUtil.getInstance().setTime(p, 20, 40, 10);
			}
			//finish game.
			GameManager.getInstance().finishGame();
		}
	}
}
