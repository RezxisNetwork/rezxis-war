package net.rezxis.war.manager.game.games;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.rezxis.war.enums.GameMode;
import net.rezxis.war.enums.Team;
import net.rezxis.war.manager.GameManager;
import net.rezxis.war.manager.PlayerManager;
import net.rezxis.war.manager.TeamManager;
import net.rezxis.war.manager.game.IGame;
import net.rezxis.war.object.WarPlayer;
import net.rezxis.war.utils.ChatUtil;
import net.rezxis.war.utils.TitleUtil;

public class DeathMatch extends IGame {

	@Getter@Setter
	private int firstTime;
	@Getter
	private Team attacking;
	
	public DeathMatch() {
		super(GameMode.DeathMatch);
	}

	@Override
	public void start() {
		attacking = Team.RED;
		ChatUtil.alertPrefix(ChatColor.RED+"先攻はREDチームです。");
	}
	
	@Override
	public void pvp() {
		
	}

	@Override
	public void finish() {
		
	}

	@Override
	public void onRespawn(PlayerRespawnEvent event) {
		event.getPlayer().setGameMode(org.bukkit.GameMode.SPECTATOR);
		event.setRespawnLocation(event.getPlayer().getLocation());
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		WarPlayer wp = PlayerManager.getInstance().getPlayer(event.getEntity());
		event.getEntity().setGameMode(org.bukkit.GameMode.SPECTATOR);
		wp.setLiving(false);
		String title = ChatColor.RED+""+TeamManager.getInstance().getRedLivingPlayers().size()+" "+ChatColor.WHITE+"- "+ChatColor.BLUE+TeamManager.getInstance().getBlueLivingPlayers().size();
		boolean end = false;
		if (wp.getTeam() == Team.RED) {
			if (TeamManager.getInstance().getRedLivingPlayers().size() == 0) {
				title = ChatColor.BLUE+"BLUEチーム"+ChatColor.GOLD+"の勝利です！";
				end = true;
			}
		} else {
			if (TeamManager.getInstance().getBlueLivingPlayers().size() == 0) {
				title = ChatColor.RED+"REDチーム"+ChatColor.GOLD+"の勝利です！";
				end = true;
			}
		}
		for (Player p : Bukkit.getOnlinePlayers()) {
			TitleUtil.getInstance().sendTitle(p, title, null, null);
			TitleUtil.getInstance().setTime(p, 20, 40, 10);
		}
		if (end) {
			GameManager.getInstance().finishGame();
		}
	}
}
