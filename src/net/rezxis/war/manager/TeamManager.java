package net.rezxis.war.manager;

import java.util.concurrent.CopyOnWriteArrayList;

import lombok.Getter;
import net.rezxis.war.enums.Team;
import net.rezxis.war.object.WarPlayer;

public class TeamManager {

	@Getter
	private static TeamManager instance;
	private CopyOnWriteArrayList<WarPlayer> red;
	private CopyOnWriteArrayList<WarPlayer> blue;
	
	public TeamManager() {
		instance = this;
		red = new CopyOnWriteArrayList<>();
		blue = new CopyOnWriteArrayList<>();
	}
	
	public boolean join(WarPlayer player, Team team) {
		if (this.getJoinnedTeam(player) != null) {
			return false;
		}
		if (team == Team.RED) {
			red.add(player);
		} else {
			blue.add(player);
		}
		player.setTeam(team);
		return true;
	}
	
	public Team getJoinnedTeam(WarPlayer player) {
		if (red.contains(player)) {
			return Team.RED;
		} else if (blue.contains(player)) {
			return Team.BLUE;
		} else {
			return null;
		}
	}
	
	public CopyOnWriteArrayList<WarPlayer> getBlueLivingPlayers() {
		CopyOnWriteArrayList<WarPlayer> r = new CopyOnWriteArrayList<WarPlayer>();
		for (WarPlayer wp : blue) {
			if (wp.isLiving())
				r.add(wp);
		}
		return r;
	}
	
	public CopyOnWriteArrayList<WarPlayer> getRedLivingPlayers() {
		CopyOnWriteArrayList<WarPlayer> r = new CopyOnWriteArrayList<WarPlayer>();
		for (WarPlayer wp : red) {
			if (wp.isLiving())
				r.add(wp);
		}
		return r;
	}
	
	public CopyOnWriteArrayList<WarPlayer> getBluePlayers() {
		return this.blue;
	}
	
	public CopyOnWriteArrayList<WarPlayer> getRedPlayers() {
		return this.red;
	}
	
	public CopyOnWriteArrayList<WarPlayer> getAllPlayers() {
		CopyOnWriteArrayList<WarPlayer> list = new CopyOnWriteArrayList<>();
		for (WarPlayer p : red)
			list.add(p);
		for (WarPlayer p : blue)
			list.add(p);
		return list;
	}
	
	public int getJoinnedPlayers() {
		return red.size()+blue.size();
	}
	
	public void clear() {
		red.clear();
		blue.clear();
	}
}
