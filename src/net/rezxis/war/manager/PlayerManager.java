package net.rezxis.war.manager;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

import lombok.Getter;
import net.rezxis.war.object.WarPlayer;

public class PlayerManager {

	@Getter
	private static PlayerManager instance;
	private HashMap<UUID,WarPlayer> players;
	
	public PlayerManager() {
		players = new HashMap<>();
		instance = this;
	}
	
	public WarPlayer getPlayer(Player player) {
		if (!players.containsKey(player.getUniqueId())) {
			players.put(player.getUniqueId(), new WarPlayer(player.getUniqueId()));
		}
		return players.get(player.getUniqueId());
	}
	
	public void clear() {
		players.clear();
	}
}
