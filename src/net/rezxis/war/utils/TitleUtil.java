package net.rezxis.war.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;

public class TitleUtil {

	@Getter
	private static TitleUtil instance;
	
	private String netMinecraftserver = "net.minecraft.server.";

	private Object enumTitle, enumSubtitle, enumActionBar;
	private Constructor<?> constructorTitle, constructorTime, constructorActionBar;
	private Method methodChatSerializer, methodHandle, methodSendpacket;
	private Field fieldConnection;

	public TitleUtil() {
		instance = this;
		try {
			String tmpPackage[] = Bukkit.getServer().getClass().getPackage().getName().split("\\.");
			String tmpVersion = tmpPackage[tmpPackage.length - 1] + ".";
			netMinecraftserver += tmpVersion;
			Class<?> tmpPacketPlayout = getNMSClass("PacketPlayOutTitle"),
					tmpIchatBase = getNMSClass("IChatBaseComponent"),
					tmpEnumTitleAction = getNMSClass("PacketPlayOutTitle$EnumTitleAction"),
					tmpChatMessageType = getNMSClass("ChatMessageType");
			enumTitle = tmpEnumTitleAction.getDeclaredField("TITLE").get(null);
			enumSubtitle = tmpEnumTitleAction.getDeclaredField("SUBTITLE").get(null);
			constructorTitle = tmpPacketPlayout.getConstructor(tmpEnumTitleAction, tmpIchatBase);
			constructorTime = tmpPacketPlayout.getConstructor(int.class, int.class, int.class);
			constructorActionBar = getNMSClass("PacketPlayOutChat").getConstructor(tmpIchatBase, tmpChatMessageType);
			enumActionBar = tmpChatMessageType.getField("GAME_INFO").get(null);
			methodChatSerializer = getNMSClass("IChatBaseComponent$ChatSerializer").getMethod("a", String.class);
			methodSendpacket = getNMSClass("PlayerConnection").getMethod("sendPacket", getNMSClass("Packet"));
			try {
				methodHandle = Class.forName("org.bukkit.craftbukkit." + tmpVersion + "entity.CraftPlayer")
						.getMethod("getHandle");
			} catch (Exception e) {
				e.printStackTrace();
			}
			fieldConnection = getNMSClass("EntityPlayer").getField("playerConnection");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void resetTitle(Player player) {
		sendTitle(player, "", "", "");
	}

	public void sendTitle(Player player, String title, String subtitle, String actionBar) {
		try {
			if (title != null) {
				sendPacket(player, constructorTitle.newInstance(
						enumTitle,
						methodChatSerializer.invoke(null, "{\"text\":\"" + title + "\"}")));
			}
			if (subtitle != null) {
				sendPacket(player, constructorTitle.newInstance(
						enumSubtitle,
						methodChatSerializer.invoke(null, "{\"text\":\"" + subtitle + "\"}")));
			}
			if (actionBar != null) {
				sendPacket(player, constructorActionBar.newInstance(
						methodChatSerializer.invoke(null, "{\"text\":\"" + actionBar + "\"}"),
						enumActionBar));
			}
		} catch (IllegalAccessException
				| InstantiationException
				| IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public void setTime(Player player, double feedIn, double titleShow, double feedOut) {
		setTime(player, (int) (feedIn * 20), (int) (titleShow * 20), (int) (feedOut * 20));
	}

	public void setTime(Player player, int feedIn, int titleShow, int feedOut) {
		try {
			sendPacket(player, constructorTime.newInstance(feedIn, titleShow, feedOut));
		} catch (IllegalAccessException
				| InstantiationException
				| IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private void sendPacket(Player player, Object packet) {
		try {
			methodSendpacket.invoke(fieldConnection.get(methodHandle.invoke(player)), packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Class<?> getNMSClass(String name) {
		try {
			return Class.forName(netMinecraftserver + name);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}