package net.rezxis.war.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rezxis.war.manager.TeamManager;
import net.rezxis.war.object.WarPlayer;

public class ChatUtil {

	public static void alert(String message) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.sendMessage(message);
		}
	}
	
	public static void alertRed(String message) {
		for (WarPlayer p : TeamManager.getInstance().getRedPlayers()) {
			Bukkit.getPlayer(p.getUuid()).sendMessage(message);
		}
	}
	
	public static void alertBlue(String message) {
		for (WarPlayer p : TeamManager.getInstance().getBluePlayers()) {
			Bukkit.getPlayer(p.getUuid()).sendMessage(message);
		}
	}
	
	public static void alertJoinned(String message) {
		for (WarPlayer p : TeamManager.getInstance().getAllPlayers()) {
			Bukkit.getPlayer(p.getUuid()).sendMessage(message);
		}
	}
	
	public static void alertPrefix(String message) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.sendMessage(ChatColor.GRAY+"["+ChatColor.GREEN+"攻城戦"+ChatColor.GRAY+"] "+message);
		}
	}
}
