package net.rezxis.war.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;

public class MapUtil {

	@Getter
	private static MapUtil instance;
	@Getter
	private JavaPlugin plugin;
	
	public MapUtil(JavaPlugin plugin) {
		instance = this;
		this.plugin = plugin;
	}
	
	public void fillSync(Location pos0, Location pos1, Material mat) {
		int x;
		int y;
		int z;
		int xx;
		int yy;
		int zz;
		if (pos0.getBlockX() <= pos1.getBlockX()) {
			x = pos0.getBlockX();
			xx = pos1.getBlockX();
		} else {
			xx = pos0.getBlockX();
			x = pos1.getBlockX();
		}
		if (pos0.getBlockY() <= pos1.getBlockY()) {
			y = pos0.getBlockY();
			yy = pos1.getBlockY();
		} else {
			yy = pos0.getBlockY();
			y = pos1.getBlockY();
		}
		if (pos0.getBlockZ() <= pos1.getBlockZ()) {
			z = pos0.getBlockZ();
			zz = pos1.getBlockZ();
		} else {
			zz = pos0.getBlockZ();
			z = pos1.getBlockZ();
		}
		System.out.println("placed "+fill(new Location(pos0.getWorld(),x,y,z),new Location(pos0.getWorld(),xx,yy,zz),mat)+" synchorously.");
	}
	
	
	private int fill(Location pos0, Location pos1, Material mat) {
		int blocks = 0;
		for (int xx = pos0.getBlockX(); xx <= pos1.getBlockX();xx++) {
			for (int yy = pos0.getBlockY(); yy <= pos1.getBlockY(); yy++) {
				for (int zz = pos0.getBlockZ(); zz <= pos1.getBlockZ(); zz++) {
					Location pos2 = new Location(pos0.getWorld(), xx, yy, zz);
					pos2.getBlock().setType(mat);
					++blocks;
				}
			}
		}
		return blocks;
	}
}
